" In vim use "zo" to open a fold, "zc" to close it.
" "zi" toggles between folden/unfolded mode.

" Modelinen and Notes {{{
" vim: set sw=4 ts=4 sts=4 et tw=78 foldmarker={{{,}}} foldlevel=0 foldmethod=marker:
"
" My ~/.vimrc :-)
"
" Author: Jens Guballa
"
" Based on the vimrc file of Amir Salihefendic, but over the time
" so much things have been adapted that now I call it MY .vimrc. :-)
"
"
" }}}

" General settings {{{

    " wildcharm
    set wildcharm=<C-z>

    " Enable modeline if present in files
    set modeline

    " Sets how many lines of history VIM has to remember
    set history=700

    " Set to auto read when a file is changed from the outside
    set autoread

    " Enable filetype plugins
    filetype plugin on
    filetype indent on

    " move cursor left/right beyond the current line places the cursor
    " in the previous/next line.
    set whichwrap+=<,>,[,]

    " Configure backspace so it acts as it should act
    set backspace=eol,start,indent

    " Configure the cursor shape
    let &t_SI = "\e[6 q" " start insert: bar
    let &t_SR = "\e[4 q" " start replace: underline
    let &t_EI = "\e[2 q" " end insert/replace: block

    " Don't use folding by default
    set nofoldenable

    " C-space toggles folds
    noremap <C-Space> za

" }}}

" Line length and indent related {{{

    " Use spaces instead of tabs
    set expandtab

    " Be smart when using tabs ;)
    set smarttab

    " 1 tab == 4 spaces
    set shiftwidth=4
    set tabstop=4

    " Never break a line autonomously.
    set linebreak
    set textwidth=0

    set ai "Auto indent
    set si "Smart indent
    set nowrap "Don't wrap lines
    set showbreak=>\

    set display+=lastline

    " Treat long lines as break lines (useful when moving around in them)
    noremap <Down> gj
    noremap <Up> gk

" }}}

" Key mappings {{{

    " With a map leader it's possible to do extra key combinations
    " like <leader>w saves the current file
    let mapleader = ","
    let g:mapleader = ","

    " Wait a little bit longer after the leader key has been pressed.
    set timeoutlen=1000

    " Fast saving
    "nmap <leader>w :w!<cr>

    " Y shal yank to the end of the line
    noremap Y y$

    " Toggle paste mode on and off
    " map <leader>pp :setlocal paste!<cr>


    " Keep the selection when (un)indenting using visual selection
    vnoremap > >gv
    vnoremap < <gv

    " Mappings for insert mode {{{
        " cursor movements in insert mode
        set winaltkeys=no
        inoremap <C-h> <Left>
        inoremap <C-j> <Down>
        inoremap <C-k> <Up>
        inoremap <C-l> <Right>

        inoremap $$ <End>
        function! MapInsertModeToggle(...)
            if a:0 != 0
                let g:map_insert_mode = a:1
            else
                " Toggle mode
                let g:map_insert_mode = 1 - get(g:, 'map_insert_mode', 0)
            endif
            if g:map_insert_mode == 1
                inoremap ' ''<left>
                inoremap " ""<left>
                inoremap ( ()<left>
                inoremap [ []<left>
                inoremap { {}<left>
            else
                iunmap '
                iunmap "
                iunmap (
                iunmap [
                iunmap {
            endif
        endfunction
        noremap <leader>cq :call MapInsertModeToggle()<CR>
        call MapInsertModeToggle(1)

    " }}}

    " Spell checking related {{{
        " Pressing ,cs will toggle and untoggle spell checking
        noremap <leader>cs :setlocal spell!<cr>

        " set language to English/German
        noremap <leader>se :setlocal spelllang=en<CR>
        noremap <leader>sd :setlocal spelllang=de<CR>

        " suggest correction:
        noremap <leader>ss z=

        " go to next/previous spelling error
        noremap <leader>sn ]s
        noremap <leader>sp [s


        noremap <leader>sa zg
    " }}}

" }}}

" User Interface related stuff {{{

    " Keep the cursor line always two lines away from top/bottom
    set scrolloff=2

    " scroll horizontylly by 1 char when cursor is 5 chars
    " away from the end of a line
    set sidescroll=1
    set sidescrolloff=5

    " Always show the status line
    set laststatus=2

    " Format the status line
    set statusline=\ %{HasPaste()}%F%m%r%h\ %w\ \ CWD:\ %r%{getcwd()}%h\ \ \ Lin/Col:\ %l/%c%(\ \ \ [%{VisualSelectionSize()}]%)

    "Always show current position
    set ruler

    " Height of the command bar
    set cmdheight=1

    " Don't redraw while executing macros (good performance config)
    set lazyredraw

    " I like line numbers.
    set number

    " Set extra options when running in GUI mode
    if has("gui_running")
        set guioptions-=T
"        set guioptions+=e
        set guioptions+=a
        set guioptions+=b
        set t_Co=256
        set guitablabel=%M\ %t
        set linespace=2
    endif

    " I would like to use the mouse
    set mouse=a

    " control behavior or right mouse button.
    " popup_setpos: set cursor to mouse pos and offer menu for word correction
    " shift-left will extend the mouse selection.
    set mousemodel=popup_setpos

    " Turn on the Wild menu
    set wildmenu

    " Ignore compiled files
    set wildignore=*.o,*~,*.pyc

    " Don't use two spaces after a dot '.' for joining lines
    set nojoinspaces

    " Toggle settings:
    " <leader>cw : "change wrap"
    noremap <leader>cw :set wrap!<cr>

    " <leader>ce : "change end of line"
    noremap <leader>ce :set list!<cr>

    " <leader>cn : "change line numbers
    noremap <leader>cn :set number!<cr>

    " <leader>cl : toggle the cursorline
    noremap <leader>cl :set cursorline!<cr>

    " <leader>cc : "change cursor column
    noremap <leader>cc :set cursorcolumn!<cr>

    " <leader>ca : toggle search wrap at end/beginning of the buffer
    noremap <leader>ca :set wrapscan!<cr>
    set wrapscan

    " <leader>cr : toggle relative line numbering
    noremap <leader>cr :set relativenumber!<cr>
    set relativenumber

" }}}

" File type specific stuff {{{

    " Python {{{
        autocmd Filetype python setlocal foldmethod=indent | set cc=88
        autocmd Filetype python noremap <buffer> <leader>p Oimport pudb; pudb.set_trace()<Esc>:w<CR>
        autocmd Filetype python noremap <buffer> <F8> :w<CR>:!black %<CR>
        autocmd Filetype python noremap <buffer> <F9> :w<CR>:cex system('mypy ' . shellescape(expand('%'))) \| copen<CR>
        autocmd Filetype python RainbowParentheses
        autocmd Filetype python noremap <buffer> <S-F7> :call flake8#Flake8UnplaceMarkers()<cr>
    " }}}

    " PHP {{{
        autocmd Filetype php RainbowParentheses
    " }}}

" }}}

" Search related settings {{{

    " Ignore case when searching
    set ignorecase

    " When searching try to be smart about cases
    set smartcase

    " Highlight search results
    set hlsearch

    " Makes search act like search in modern browsers
    set incsearch

    " For regular expressions turn magic on
    set magic

    " Show matching brackets when text indicator is over them
    set showmatch
    " How many tenths of a second to blink when matching brackets
    set mat=2

    " No annoying sound on errors

    set novisualbell
    set t_vb=
    set tm=500

    " Visual mode pressing * or # searches for the current selection
    " Super useful! From an idea by Michael Naumann
    vnoremap <silent> * :call VisualSelection('f')<CR>
    vnoremap <silent> # :call VisualSelection('b')<CR>

    " Map <Space> to / (search) and <leader><Space> to ? (backwards search)
    " Let's be very magic: Regex behaviour shall be like for PCRE
    noremap <space> /\v
    noremap <leader><space> ?\v

    " Search very magic within the (lastly selected) visual selection
    "map <leader><space> :/\%V\v

    " Disable highlight when <leader><cr> is pressed
    noremap <silent> <leader><cr> :noh<cr>:echon ''<cr>

" }}}

" Visual selection and clipboard related settings {{{

    " Yanked text shall go into the clipboard
    set clipboard=autoselect,unnamedplus,unnamed

    " Allow virtual blocks to exceed the line length.
    set virtualedit+=block

    " When yanking the visual selection, the cursor shall not move
    vnoremap y ygv<Esc>

    " Use Ctrl-LeftMouse for block selection
    noremap <C-LeftMouse> <LeftMouse><Esc><C-V>
    noremap <C-LeftDrag> <LeftDrag>

    " copy selected text to system clipboard
    "vmap q ygv

    " select all
    noremap <leader>a <ESC>ggVG$

    "" And here is another way of using the system clipboard
    vmap <leader>y ygv
    "map <leader>p P<Right>
    "vnoremap <leader>x "+d
    "noremap <leader>a <ESC>ggVG$

    "" Select the lastly pasted text
    noremap <leader>v `[v`]

    "" Replace selection by 0-register
    vnoremap . "0p

" }}}

" File related settings {{{

    " Turn backup off, since most stuff is in SVN, git et.c anyway...
    set nobackup
    set nowb
    set noswapfile

    " Set utf8 as standard encoding and en_US as the standard language
    set encoding=utf8

    " Use Unix as the standard file type
    set ffs=unix,dos,mac

    noremap <leader><leader>h :help jens_help.txt<CR>
    noremap <leader><leader>g :execute ":e " . resolve(expand("~/.vimrc"))<CR>
    noremap <leader><leader>j :e ~/.vim/doc/jens_help.txt<CR>

    autocmd ColorScheme * highlight ExtraWhitespace ctermbg=yellow guibg=LightYellow
    au InsertLeave * match ExtraWhitespace /\s\+$/

" }}}

" Colors and themes {{{

    " Enable syntax highlighting
    syntax enable
    syntax sync fromstart
    "let g:lucius_style = 'light'
    "let g:lucius_contrast = 'high'
    set background=light

    colorscheme lucius
    " if has("gui_running")
    "     " colorscheme darkblue
    "     " highlight Visual guifg=#6060c0 guibg=#600060
    "     colorscheme lucius
    " else
    "     colorscheme lucius
    " endif


    " highlight cursor line
    " highlight CursorLine guibg=#101050
    " highlight CursorColumn guibg=#101050
    set cursorline
    hi WarningMsg ctermfg=white ctermbg=red guifg=White guibg=Red gui=None

    " Choose the color for the line numbers
    " highlight LineNr term=bold cterm=NONE ctermfg=DarkGrey ctermbg=NONE gui=NONE guifg=#8000C0 guibg=NONE
" }}}

" Window, tab and buffer related settings {{{

    " A buffer becomes hidden when it is abandoned
    set hidden

    " Smart way to move between windows
    nnoremap <C-j> <C-W>j
    nnoremap <C-k> <C-W>k
    nnoremap <C-h> <C-W>h
    nnoremap <C-l> <C-W>l

    " show all buffers and ask for the buffer to switch to
    noremap <leader>l :buffers<cr>:b<Space>
    " map <leader>bb :buffers!<cr>:b<Space>

    " Close buffer but keep window open
    noremap <leader>bd :bn <BAR> :bd #<CR>
    noremap <leader>bb :bn <BAR> :bd #<CR>

    noremap <leader>q :bn <BAR> :bd #<CR>

    " Open a new buffer with the current buffer's path
    noremap <leader>e :Lexplore<cr>

    " Open current buffer in a new tab
    noremap <leader>t :tab split<CR>
    noremap <leader>tt :tabclose<CR>
    noremap <leader><leader>t :tabclose<CR>

    " Cycle trough buffer list
    " map <leader>bn :bnext<cr>
    " map <leader>bp :bprevious<cr>
    noremap <C-PageDown> :bnext<cr>
    noremap <C-PageUp> :bprevious<cr>

    noremap <C-n> :bp<CR>
    noremap + :bn<CR>

    " Switch CWD to the directory of the open buffer
    noremap <leader>cd :cd %:p:h<cr>:pwd<cr>

    " Specify the behavior when switching between buffers
    try
        set switchbuf=useopen,usetab,newtab
        set stal=2
    catch
    endtry

    " Return to last edit position when opening files (You want this!)
    autocmd BufReadPost *
         \ if line("'\"") > 0 && line("'\"") <= line("$") |
         \   exe "normal! g`\"" |
         \ endif
    " Remember info about open buffers on close
    set viminfo^=%
" }}}

" Plugins and other vim files {{{

    set nocompatible              " be iMproved, required
    filetype off                  " required

    " set the runtime path to include Vundle and initialize
    set rtp+=~/.vim/bundle/Vundle.vim
    call vundle#begin()
    " alternatively, pass a path where Vundle should install plugins
    "call vundle#begin('~/some/path/here')

    " Plugin Vundle: Bundle manager {{{
        " let Vundle manage Vundle, required
        Plugin 'VundleVim/Vundle.vim'
    " }}}

    " Plugin indentLine: Display helper lines vertically for each indent level {{{
    "   Plugin 'Yggdroot/indentLine'
    "   " toggle display of help lines
    "   map <leader>ci :IndentLinesToggle<CR>
    "   let g:indentLine_enabled = 0
    " }}}

    " Plugin airline themes: several themes for the airline status line {{{
       Plugin 'vim-airline/vim-airline-themes'
    "}}}

    " Plugin airline: ultra cool layout of the status line ;-) {{{
        Plugin 'vim-airline/vim-airline'

        " No z-section, no warning section.
        let g:airline#extensions#default#layout = [
              \ [ 'a', 'b', 'c' ],
              \ [ 'x', 'y', 'z']
              \ ]
        "let g:airline#extensions#default#section_truncate_width = {
        "      \ 'c': 20,
        "      \ 'x': 60,
        "      \ 'y': 60,
        "      \ }
        " if &background == 'dark'
        "     let g:airline_theme='molokai'
        " else
        "     let g:airline_theme='murmur'
        " endif
        let g:airline_theme='murmur'

        "let g:airline_powerline_fonts = 1
        let g:airline_left_sep = '|'
        let g:airline_left_alt_sep = '|'
        let g:airline_right_sep = '|'
        let g:airline_right_alt_sep = '|'
        let g:airline#extensions#whitespace#enabled = 1
        let g:airline#extensions#hunks#enabled=0
        let g:airline#extensions#branch#enabled=1
        "let g:airline_section_b="L/C: %-3l %-3c %3p%%%( [%{VisualSelectionSize()}]%)"
        let g:airline_sec_a_orig = ""
        "let g:airline_section_c="%p%% L:%l C:%c%( [%{VisualSelectionSize()}]%)%=%t%m%( %r)"
        let g:airline_section_z="%p%% L:%l C:%c%( %r)"

        " Enable the list of buffers
        let g:airline#extensions#tabline#enabled = 1

        " Show just the filename
        let g:airline#extensions#tabline#fnamemod = ':t'
        function! AirlineInit()
            if g:airline_sec_a_orig == ""
                let g:airline_sec_a_orig = g:airline_section_a
            endif
            let g:airline_section_a = g:airline_sec_a_orig . '%( [%{VisualSelectionSize()}]%)'
        endfunction
    " }}}

    " Plugin jens_collection: functions to split a line into fixed block widths {{{
        set rtp+=~/.vim/bundle/jens_collection
        " Provides the command "Blocksplit <from-width> [<to-width>]
        call jens_collection#init()
    " }}}

    " Plugin visualshift: Allow comfortable shift of visual selection {{{
        set rtp+=~/.vim/bundle/visualshift
        let g:visualshift#default_map = 0       " we define our own key mapping
        call visualshift#init()
        vmap <C-l> :call VisualShiftRight()<CR>
        vmap <C-h> :call VisualShiftLeft()<CR>
        vmap <C-k> :call VisualShiftUp()<CR>
        vmap <C-j> :call VisualShiftDown()<CR>
    " }}}

    " Plugin bufexplorer: comfortable way to switch between buffers {{{
        Plugin 'jlanzarotta/bufexplorer'
        nnoremap <leader><leader>b :BufExplorer<CR>
    " }}}

    " Plugin Mark--Karkat : Mark selected text {{{
        Plugin 'Mark--Karkat'
        nnoremap <leader>N <Plug>MarkClear
        nmap <leader>m <Plug>MarkSet
        vmap <leader>m <Plug>MarkSet
        nmap <leader>n <Plug>MarkAllClear
        noremap <leader>cm <Plug>MarkToggle
        nmap <leader>r <Plug>MarkRegex
        vmap <leader>r <Plug>MarkRegex
    " }}}

    " Plugin Tagbar {{{
        Plugin 'majutsushi/tagbar'
        noremap <leader>ct :TagbarToggle<CR>
        set ut=1000
    " }}}

    " Plugin CtrlP {{{
        Plugin 'kien/ctrlp.vim'

        let g:ctrlp_open_multiple_files = '1vjr'
        let g:ctrlp_show_hidden = 1
        let g:ctrlp_cmd = 'CtrlPMRU'
        " let g:ctrlp_prompt_mappings = {
        "   \ 'PrtHistory(-1)':       ['<c-b>'],
        "   \ 'PrtHistory(1)':        ['<c-f>'],
        "   \ 'ToggleType(1)':        ['<c-p>'],
        "   \ 'ToggleType(-1)':       ['<c-n>']
        "   \ }
    " }}}

    " Plugin Supertab: autocompletion via tab {{{
        Plugin 'ervandew/supertab'
        "let g:SuperTabContextDefaultCompletionType = "<c-x><c-o>"
        "let g:SuperTabDefaultCompletionType = "context"
    " }}}


    " Plugin Jedivim: broad python support {{{
    "   Plugin 'davidhalter/jedi-vim'
    "
    "   let g:jedi#usages_command = '<leader>k'
    "   let g:jedi#popup_on_dot = 0
    "   let g:jedi#completions_enabled = 0
    " }}}

    " Plugin Vim_flake: {{{
       Plugin 'nvie/vim-flake8'

       let g:flake8_show_in_gutter = 1
       let g:flake8_error_marker='EE'       " set error marker to 'EE'
       let g:flake8_warning_marker='WW'     " set warning marker to 'WW'
       let g:flake8_pyflake_marker='FF'     " disable PyFlakes warnings
       let g:flake8_complexity_marker='CC'  " disable McCabe complexity warnings
       let g:flake8_naming_marker='NN'      " disable naming warnings

       " autocmd BufWritePost *.py call flake8#Flake8()
       command! Flake8 call flake8#Flake8()

    " }}}

    " Plugin Black: Python formatter {{{
    "    Plugin 'psf/black'
    " }}}

    " Plugin UltiSnips: Snippets {{{
    "   Plugin 'SirVer/ultisnips'
    "
    "   let g:UltiSnipsExpandTrigger="<leader><tab>"
    "   ""let g:UltiSnipsJumpForwardTrigger="<c-p>"
    "   ""let g:UltiSnipsJumpBackwardTrigger="<c-b>"
    "
    "   " If you want :UltiSnipsEdit to split your window.
    "   let g:UltiSnipsEditSplit="vertical"
    "
    "   noremap <leader>u :UltiSnipsEdit<CR>
    "   noremap <leader>uu :UltiSnipsEdit!<CR>
    " }}}

    " Plugin Nerdtree: {{{
    "    Plugin 'preservim/nerdtree'
    " }}}

    " Plugin Fugitive: git support {{{
        Plugin 'tpope/vim-fugitive'
        let g:fugitive_no_maps = 1
    " }}}

    " Plugin Python_mode: python IDE {{
    "    Plugin 'python-mode/python-mode'
    " }}}

    " Plugin Rainbow_parantheses: highlighting of matching brackets {{{
        Plugin 'junegunn/rainbow_parentheses.vim'

        let g:rainbow#pairs = [['(', ')'], ['[', ']'], ['{', '}']]

        noremap <leader>cp :RainbowParentheses!!<CR>

    " }}}

    " Plugin Snytastic: syntax checker {{{
    "    Plugin 'vim-syntastic/syntastic'
    " }}}

    " Plugin Vim_Colorschemes: multiple schemes {{{
        Plugin 'rafi/awesome-vim-colorschemes'

    " }}}

    call vundle#end()            " required
    filetype plugin indent on    " required

    autocmd VimEnter * call AirlineInit()
" }}}

" vimgrep. I don't use it yet. {{{
    " When you press gv you vimgrep after the selected text
    vnoremap <silent> gv :call VisualSelection('gv')<CR>

    " Open vimgrep and put the cursor in the right position
    " map <leader>g :vimgrep // **/*.<left><left><left><left><left><left><left>

    " Vimgreps in the current file
    " map <leader><space> :vimgrep // <C-R>%<C-A><right><right><right><right><right><right><right><right><right>

    " When you press <leader>r you can search and replace the selected text
    " vnoremap <silent> <leader>r :call VisualSelection('replace')<CR>

" }}}

" Helper Functions {{{

    " Function CmdLine {{{
    function! CmdLine(str)
        exe "menu Foo.Bar :" . a:str
        emenu Foo.Bar
        unmenu Foo
    endfunction
    " }}}

    " Function VisualSelection {{{
    function! VisualSelection(direction) range
        let l:saved_reg = @"
        execute "normal! vgvy"

        let l:pattern = escape(@", '\\/.*$^~[]')
        let l:pattern = substitute(l:pattern, "\n$", "", "")

        if a:direction == 'b'
            execute "normal ?" . l:pattern . "^M"
        elseif a:direction == 'gv'
            call CmdLine("vimgrep " . '/'. l:pattern . '/' . ' **/*.')
        elseif a:direction == 'replace'
            call CmdLine("%s" . '/'. l:pattern . '/')
        elseif a:direction == 'f'
            execute "normal /" . l:pattern . "^M"
        endif

        let @/ = l:pattern
        let @" = l:saved_reg
    endfunction
    " }}}

    " Function HasPaste {{{
    " Returns true if paste mode is enabled
    function! HasPaste()
        if &paste
            return 'PASTE MODE  '
        en
        return ''
    endfunction
    " }}}

    " Function BufcloseCloseIt {{{
    " Don't close window, when deleting a buffer
    command! Bclose call <SID>BufcloseCloseIt()
    function! <SID>BufcloseCloseIt()
       let l:currentBufNum = bufnr("%")
       let l:alternateBufNum = bufnr("#")

       if &modified
         echohl ErrorMsg | echo "No write since last change for buffer." | echohl None
       else
         if buflisted(l:alternateBufNum)
           buffer #
         else
           bnext
         endif

         if bufnr("%") == l:currentBufNum
           new
         endif
         if buflisted(l:currentBufNum)
           execute("bdelete ".l:currentBufNum)
         endif
       endif
    endfunction
    " }}}

    " Function VisualSelectionSize {{{
    " Function to display the character count (or line count) for selections
    function! VisualSelectionSize()
        if mode() == "v"
            if line(".") != line("v")
                return (abs(line(".") - line("v")) + 1) . ' lines'
            else
                return (abs(col(".") - col("v")) + 1) . ' chars'
            endif
        elseif mode() == "V"
            return (abs(line(".") - line("v")) + 1) . ' lines'
        elseif mode() == "\<C-V>"
            return (abs(line(".") - line("v")) + 1) . 'x' . (abs(col(".") - col("v")) + 1) . ' block'
        else
            return ''
        endif
    endfunction
    " }}} VisualSelectionSize
" }}}
